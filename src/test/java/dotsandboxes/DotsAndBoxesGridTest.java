package dotsandboxes;

import static org.junit.jupiter.api.Assertions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

public class DotsAndBoxesGridTest {

  /*
   * Because Test classes are classes, they can have fields, and can have static fields.
   * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
   */
  private static final Logger logger = LogManager.getLogger(
    DotsAndBoxesGridTest.class
  );

  /*
   * Tests are functions that have an @Test annotation before them.
   * The typical format of a test is that it contains some code that does something, and then one
   * or more assertions to check that a condition holds.
   *
   * This is a dummy test just to show that the test suite itself runs
   */
  @Test
  public void testTestSuiteRuns() {
    logger.info("Dummy test to show the test suite runs");
    assertTrue(true);
  }

  final int width = 5;
  final int height = 5;

  // FIXME: You need to write tests for the two known bugs in the code.
  @Test
  public void boxCompleteDetectsCompletedBoxes() {
    /*
     * Box is complete
     */
    DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(width, height, 2);
    case1.drawHorizontal(0, 0, 0);
    case1.drawVertical(0, 0, 1);
    case1.drawHorizontal(0, 1, 0);
    case1.drawVertical(1, 0, 1);
    assertTrue(case1.boxComplete(0, 0));
  }

  @Test
  public void boxCompleteDetectsIncompleteBoxes() {
    /*
     * Box is incomplete
     */
    DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(width, height, 2);
    case1.drawHorizontal(0, 0, 0);
    case1.drawVertical(0, 0, 1);
    case1.drawHorizontal(0, 1, 0);
    assertFalse(case1.boxComplete(0, 0));
  }

  @Test
  public void boxCompleteStaysWithinBounds() {
    /*
     * Box is within game board bounds
     */
    DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(width, height, 2);
    assertFalse(case1.boxComplete(-1, 0));
    assertFalse(case1.boxComplete(width, 0));
    assertFalse(case1.boxComplete(0, height));
    assertFalse(case1.boxComplete(0, -1));
  }

  @Test
  public void drawMethodsDetectRedrawnLines() {
    DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(width, height, 2);

    /*
     * Drawn lines are not duplicated
     */

    case1.drawHorizontal(0, 0, 0);

    assertThrows(
      RuntimeException.class,
      () -> {
        case1.drawHorizontal(0, 0, 0);
      }
    );

    case1.drawVertical(0, 0, 0);

    assertThrows(
      RuntimeException.class,
      () -> {
        case1.drawVertical(0, 0, 0);
      }
    );
  }

  @Test
  public void drawMethodsStayWithinBounds() {
    DotsAndBoxesGrid case1 = new DotsAndBoxesGrid(width, height, 2);

    /*
     * Drawn lines are within game board bounds
     */

    assertThrows(
      IndexOutOfBoundsException.class,
      () -> {
        case1.drawHorizontal(-1, 0, 0);
      }
    );

    assertThrows(
      IndexOutOfBoundsException.class,
      () -> {
        case1.drawHorizontal(width, 0, 0);
      }
    );

    assertThrows(
      IndexOutOfBoundsException.class,
      () -> {
        case1.drawVertical(0, -1, 0);
      }
    );

    assertThrows(
      IndexOutOfBoundsException.class,
      () -> {
        case1.drawVertical(0, height, 0);
      }
    );
  }
}
